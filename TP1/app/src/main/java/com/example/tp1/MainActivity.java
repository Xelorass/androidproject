package com.example.tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    public static int count = 0;
    private static final String FILE_NAME = "example.txt";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final SQLiteDB  dbUser = new SQLiteDB(MainActivity.this);
        final SQLiteDatabase my_bdd = dbUser.getWritableDatabase();

        //Bouton de lien vers la listes des utilisateurs
        findViewById(R.id.viewlist).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MainActivity.this, ListUser.class);
                        startActivity(intent);
                    }
                });

        //Bouton d'ajout d'un utilisateur
        findViewById(R.id.adduser).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Récupération desinput pour la création
                        EditText FNobj = findViewById(R.id.InputFN);
                        EditText LNobj = findViewById(R.id.InputLN);
                        EditText Phoneobj = findViewById(R.id.InputPhone);
                        //Récupération des valeurs des input
                        String FN = String.valueOf(FNobj.getText());
                        String LN = String.valueOf(LNobj.getText());
                        String Phone = String.valueOf(Phoneobj.getText());

                        //Si un ou plusieurs des input est/sont vide, n'envoie rien
                        if (!FN.equals("") && !LN.equals("") && !Phone.equals("")){

                            /** Inutile avec la création de la BDD */
                            String newUser = FNobj.getText()+"_"+LNobj.getText()+"_"+Phoneobj.getText()+"\n";

                            Intent intent = new Intent(MainActivity.this, ListUser.class);

                            /** ***************** EXO 3 enlever lors de la mis en place de la bdd
                            //Ajout du user dans le fichier de save
                            FileOutputStream fos = null;

                            try {
                                fos = openFileOutput(FILE_NAME, MODE_APPEND);
                                fos.write(newUser.getBytes());

                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            } finally {
                                if (fos != null) {
                                    try {
                                        fos.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                             *********************************************************************/

                            /** EXO 2 remplacer par le fichier de save
                            intent.putExtra("newuser", newUser);
                            *****************************************/
                            SQLiteDB.addUser(FN, LN, Phone, my_bdd);
                            startActivity(intent);
                        } else {
                            Toast.makeText(MainActivity.this, "formulaire invalide", Toast.LENGTH_SHORT).show();
                        }


                    }
                });
    }


    @Override
    protected void onResume(){
        super.onResume();
        //implémente le compteur pour le nombre de visite de l'activité principale de l'appli
        count = count + 1;
        TextView countView = findViewById(R.id.nbVisu);
        String display_count = "x"+count;
        countView.setText(display_count);


    }

}
