package com.example.tp1;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

class SQLiteDB extends SQLiteOpenHelper {

    private static final int BASE_VERSION = 1;
    private static final String BASE_NOM = "user.db";
    private static final String TABLE_USERS = "table_user";
    private static final String COLONNE_ID = "id";
    private static final String COLONNE_FN = "firstname";
    private static final String COLONNE_LN = "lastname";
    private static final String COLONNE_PHONE = "phone";

    /** * La requête de création de la structure de la base de données. */
    private static final String REQUETE_CREATION_BD = "create table " + TABLE_USERS + " ("
            + COLONNE_ID + " integer primary key autoincrement, "
            + COLONNE_FN + " text not null, "
            + COLONNE_LN + " text not null, "
            + COLONNE_PHONE + " text not null" +
            ");";
    /** * La requête de destruction de la structure de la base de données */
    private static final String REQUETE_DESTROY_BD = "DROP TABLE" + TABLE_USERS + ";";

    public SQLiteDB(Context context) {
        super(context, BASE_NOM, null, BASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(REQUETE_CREATION_BD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) { //Dans notre cas, nous supprimons la base et les données pour en // créer une nouvelle ensuite.
        db.execSQL(REQUETE_DESTROY_BD); // Création de la nouvelle structure.
        onCreate(db);
    }

    public static int getBaseVersion() {
        return BASE_VERSION;
    }

    public static String getBaseNom() {
        return BASE_NOM;
    }

    public static void addUser(String fn, String ln, String phone, SQLiteDatabase db){
        db.execSQL("INSERT INTO "+TABLE_USERS+" ("+COLONNE_FN+", "+COLONNE_LN+", "+COLONNE_PHONE+") VALUES ('"+fn+"', '"+ln+"', '"+phone+"');");
    }
}
