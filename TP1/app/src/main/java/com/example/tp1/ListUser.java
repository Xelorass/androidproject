package com.example.tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class ListUser extends AppCompatActivity {

    private ArrayList<String> allUser = new ArrayList<>();
    private static final String FILE_NAME = "example.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_user);

        final SQLiteDB  dbUser = new SQLiteDB(ListUser.this);
        final SQLiteDatabase my_bdd = dbUser.getReadableDatabase();

        //récupération dans un ArrayList de tout les users du fichier de save
        /** ******* EXO 3 enlever lors de la mis en place de la bdd
        FileInputStream fis = null;

        try {
            fis = openFileInput(FILE_NAME);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            String text;

            while ((text = br.readLine()) != null) {
                allUser.add(text);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        **********************************************/

        /** ***********EXO 2 remplacer par le fichier de save
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            allUser.add(extras.getString("newuser"));
        }
        ********************************************/
        //écriture de la liste de user en fonction de l'ArrayList récupéré précedement
        ArrayAdapter<String> adaptater = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, allUser
        );

        displayData(my_bdd);
        ListView listView = findViewById(R.id.listUser);
        listView.setAdapter(adaptater);

        //bouton de retour a l'ajout de user
        findViewById(R.id.retour).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(ListUser.this, MainActivity.class);
                        startActivity(intent);
                    }
                });
    }
    //fonction appelé par le bouton reset qui vide
    /** ************* Utile seulement pour la version avec fichier .txt */
    @SuppressLint("Assert")//je sais pas a quoi ça sers mais ça enlève les points jaunes
    public void emptyFile(View v){

        String text = null;
        FileOutputStream fos = null;

        try {
            fos = openFileOutput(FILE_NAME, MODE_PRIVATE);
            assert false;   //je sais pas a quoi ça sers mais ça enlève le point jaune du getBytes pour les retours
                            //pour les retours de valeurs null
            fos.write(text.getBytes());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        Toast.makeText(ListUser.this, "Reset des users effectué", Toast.LENGTH_LONG).show();
    }

    private void displayData(SQLiteDatabase db) {
        Cursor cursor = db.rawQuery("SELECT * FROM table_user;",null);
        allUser.clear();
        if (cursor.moveToFirst()) {
            do {
                String cnom = cursor.getString(cursor.getColumnIndex("firstname"));
                String cprenom = cursor.getString(cursor.getColumnIndex("lastname"));
                String ctel = cursor.getString(cursor.getColumnIndex("phone"));
                String contact_concatene = cnom.concat("_").concat(cprenom).concat(" : ").concat(ctel);
                allUser.add(contact_concatene);
            } while (cursor.moveToNext());
        }

        cursor.close();
    }
}
